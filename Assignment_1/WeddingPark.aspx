﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WeddingPark.aspx.cs" Inherits="Assignment_1.WeddingPark" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
             <h1>Wedding Time</h1>
     
        <br />
            <div>
                <asp:Label runat="server" Text="Title:- " AssociatedControlID="title"></asp:Label>
                <asp:DropDownList runat="server" ID="title">
                    <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
                    <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                    <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
                </asp:DropDownList>

                <asp:Label runat="server" Text="First Name: " AssociatedControlID="clientFName" ID="clientFNameLabel"></asp:Label> 
                <asp:TextBox runat="server" ID="clientFName" placeholder="First Name" CausesValidation="true" ></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter First Name!!" ControlToValidate="clientFName" ID="validatorFName"></asp:RequiredFieldValidator>
            
                <asp:Label runat="server" Text="Last Name: " ID="clientLNameLabel" AssociatedControlID="clientLName"></asp:Label>
                <asp:TextBox runat="server" ID="clientLName" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Last Name!!" ControlToValidate="clientLName" ID="validatorLName"></asp:RequiredFieldValidator>
        </div>
        
            
            
        <br />
            <asp:Label runat="server" Text="Email: " ID="clientEmaillabel" AssociatedControlID="Email"></asp:Label>
            <asp:TextBox runat="server" ID="Email" placeholder="Email" ></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="validatorEmail" ControlToValidate="Email" ErrorMessage="Invalid Email format!!!!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator   runat="server" ControlToValidate="Email" ErrorMessage="Please Enter Email.."></asp:RequiredFieldValidator>
        
        <br />
             <asp:Label runat="server" Text="Confirm Email*: " ID="confirmEmailLabel" AssociatedControlID="confirmEmail"></asp:Label>
            <asp:TextBox runat="server" ID="confirmEmail" placeholder="Email"></asp:TextBox>
            <asp:CompareValidator runat="server" ID="compareEmail" ErrorMessage="Email do no match" ControlToCompare="Email" ControlToValidate="confirmEmail"></asp:CompareValidator>
            <asp:RequiredFieldValidator   runat="server" ControlToValidate="confirmEmail" ErrorMessage="Please Enter Email.."></asp:RequiredFieldValidator>
    
        <br />
            <asp:Label runat="server" ID="contactLabel" Text="Contact No: " AssociatedControlID="phone"></asp:Label>
            <asp:TextBox runat="server" ID="phone" placeholder="Contact No"></asp:TextBox>
           
         
        <div>
            <asp:Label runat="server" Text="Venue:- " AssociatedControlID="venue"></asp:Label>
                <asp:DropDownList runat="server" ID="venue">
                    <asp:ListItem Text="Toronto Island Park" Value="Toronto Island Park"></asp:ListItem>
                    <asp:ListItem Text="Edward Gardens " Value="Edward Gardens"></asp:ListItem>
                    <asp:ListItem Text="Woodbine Park" Value="Woodbine Park"></asp:ListItem>
                </asp:DropDownList>
        </div>
        <div>
            <asp:Label ID="foodFacilityLabel" Text="Facility : " runat="server"></asp:Label>
             <div id="Facility" runat="server">
            <asp:CheckBox runat="server" ID="breakfast" Text="Breakfast" />
            <asp:CheckBox runat="server" ID="lunch" Text="Lunch" />
            <asp:CheckBox runat="server" ID="dinner" Text="Dinner" />
            <asp:CheckBox runat="server" ID="music" Text="Music" />
            <asp:CheckBox runat="server" ID="interior" Text="Interior" />
            <asp:CheckBox runat="server" ID="travel" Text="Travel" />
             </div>
        </div>
        <div>
            <asp:Label runat="server" ID="paymentModeLabel" Text="Mode of Payment: " AssociatedControlID="paymentMode"></asp:Label>
            <asp:RadioButtonList runat="server" ID="paymentMode">
                <asp:ListItem runat="server" Text="Debit card" GroupName="paymentMode" />
                <asp:ListItem runat="server" Text="Online banking" GroupName="paymentMode"/>
                <asp:ListItem runat="server" Text="Cash At office" GroupName="paymentMode"/>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ID="validatorfielPaymentMode" ControlToValidate="paymentMode" ErrorMessage="Please select Payment mode"></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Button Text="Submit" runat="server" ID="submitButton" />
        </div>
        <asp:ValidationSummary runat="server" ID="summary" HeaderText="must enter following fields:" />
    </form>
</body>
</html>
